import styled from 'styled-components'
import Avatar from '../Avatar'

export default styled(Avatar)`
  float: left,
  display: block,
  margin-right: 15px,
  box-shadow: 0px 0px 0px 8px rgba(0,0,0,0.2)
`
