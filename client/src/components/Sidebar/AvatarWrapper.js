import styled from 'styled-components'

export default styled.div`
  display: flex;
  justify-content: space-around;
  padding: 15px 0 20px 0;
  background-image: url('${require('../../images/material_bg.png')}');
  height: 45px;
`
