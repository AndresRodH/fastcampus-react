import styled from 'styled-components'
import MenuItem from 'material-ui/MenuItem'

export default styled(MenuItem)`
  color: white !important;
  font-size: 14px;
`
