import styled from 'styled-components'

export default styled.span`
  padding-top: 12px;
  display: block;
  color: white;
  font-weight: 300;
  text-shadow: 1px 1px #444;
`
