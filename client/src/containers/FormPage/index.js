import React from 'react'
import { Link } from 'react-router-dom'
import RaisedButton from 'material-ui/RaisedButton'
import MenuItem from 'material-ui/MenuItem'
import TextField from 'material-ui/TextField'
import SelectField from 'material-ui/SelectField'
import Toggle from 'material-ui/Toggle'
import DatePicker from 'material-ui/DatePicker'
import { grey400 } from 'material-ui/styles/colors'
import Divider from 'material-ui/Divider'
import Base from '../../components/Base'

const FormPage = () => {
  const styles = {
    toggleDiv: {
      maxWidth: 300,
      marginTop: 40,
      marginBottom: 5
    },
    toggleLabel: {
      color: grey400,
      fontWeight: 100
    },
    buttons: {
      marginTop: 30,
      float: 'right'
    },
    saveButton: {
      marginLeft: 5
    }
  }

  return (
    <Base title='Form Page'
      navigation='Application / Form Page'>
      <form>

        <TextField
          hintText='Name'
          floatingLabelText='Name'
          fullWidth
        />

        <SelectField
          floatingLabelText='City'
          value=''
          fullWidth>
          <MenuItem key={0} primaryText='London' />
          <MenuItem key={1} primaryText='Paris' />
          <MenuItem key={2} primaryText='Rome' />
        </SelectField>

        <DatePicker
          hintText='Expiration Date'
          floatingLabelText='Expiration Date'
          fullWidth />

        <div style={styles.toggleDiv}>
          <Toggle
            label='Disabled'
            labelStyle={styles.toggleLabel}
          />
        </div>

        <Divider />

        <div style={styles.buttons}>
          <Link to='/'>
            <RaisedButton label='Cancel' />
          </Link>

          <RaisedButton label='Save'
            style={styles.saveButton}
            type='submit'
            primary />
        </div>
      </form>
    </Base>
  )
}

export default FormPage
